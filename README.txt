Flagging Modal Forms
====================

This module, like the flagging_dialog module which comes as part of
[Flagging Form][1], displays the form to create/edit/delete a [flagging][2] in a
modal dialog. Unlike that module, this one uses [CTools][3] (possibly via
[Modal Forms][4]) instead of [Dialog][5].

[1]: http://drupal.org/project/flagging_form
[2]: http://drupal.org/project/flag
[3]: http://drupal.org/project/ctools
[4]: http://drupal.org/project/modal_forms
[5]: http://drupal.org/project/dialog

Status
------

This module currently does nothing useful; it's just a copy of the code from
flagging_dialog with some strings replaced.
